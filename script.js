state = "X"
count = 0;
winner = false
function played(id_txt) {
    count++;
    if (document.getElementById(id_txt).style.disabled) {
        alrt();
        console.log(id_txt + " played? " + document.getElementById(id_txt).style.disabled)
    } else {
        document.getElementById(id_txt).style.disabled = true
        document.getElementById(id_txt).innerText = state
        document.getElementById(id_txt).style.boxShadow = "inset 4px 4px 10px 0 rgba(0, 0, 0, 0.4), inset -4px -4px 10px 0 rgba(100, 100, 100, 0.70)"
        document.getElementById(id_txt).style.color = "#0066ff"
        document.getElementById(id_txt).style.fontWeight = "bold"

        console.log(id_txt, state);
        if (state == "X") {
            state = "O"
            document.form.textview.value = state + " is now playing";
        } else {
            state = "X"
            document.form.textview.value = state + " is now playing";
        }
    }
    chck();
    tie();
}

function alrt() {
    swal({
        title: "NOPE!",
        text: "This has already been played!",
        icon: "warning",
    });
}

function gover() {
    swal({
        title: "TIE!",
        text: "No winners!",
        icon: "warning",
    });
}

function won(wtxt) {
    winner = true;
    var won = ""
    if (wtxt == "X") {
        won = "1st player won!"
    } else if (wtxt == "O") {
        won = "2nd player won!"
    }
    swal({
        title: "Congratulations!",
        text: won,
        icon: "success",
    });
    document.getElementById("one").style.disabled = true
    document.getElementById("two").style.disabled = true
    document.getElementById("three").style.disabled = true
    document.getElementById("four").style.disabled = true
    document.getElementById("five").style.disabled = true
    document.getElementById("six").style.disabled = true
    document.getElementById("seven").style.disabled = true
    document.getElementById("eight").style.disabled = true
    document.getElementById("nine").style.disabled = true
}

function chck() {
    let op_w = [["one", "two", "three"], ["four", "five", "six"], ["seven", "eight", "nine"], ["one", "five", "nine"], ["seven", "five", "three"], ["one", "four", "seven"], ["two", "five", "eight"], ["three", "six", "nine"]]

    try {
        for (var i = 0; i < op_w.length; i++) {
            //console.log(op_w[i][0],op_w[i][1],op_w[i][2])
            if (document.getElementById(op_w[i][0]).style.disabled && document.getElementById(op_w[i][1]).style.disabled && document.getElementById(op_w[i][2]).style.disabled) {
                if (document.getElementById(op_w[i][0]).innerText == document.getElementById(op_w[i][1]).innerText && document.getElementById(op_w[i][0]).innerText == document.getElementById(op_w[i][2]).innerText) {
                    console.log("won = " + document.getElementById(op_w[i][0]).innerText);
                    won(document.getElementById(op_w[i][0]).innerText);
                    break;
                }
            }
        }

    } catch (err) {
        if (err instanceof TypeError) {
            console.log(err);
            alert("An error has been produced.\nPage will be reloaded")
            location.reload();
        }
    }

}


function tie (){
    print(count)
    if (count >= 9 && winner == false) {
        gover();
    }
}

function print(x){
    console.log(x);
}